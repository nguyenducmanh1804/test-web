$(document).ready(function () {
  $(".alert-autocloseable-success").hide();
  $(".alert-autocloseable-warning").hide();
  $(".alert-autocloseable-danger").hide();
  $(".alert-autocloseable-info").hide();

  $("#autoclosable-btn-success").click(function () {
    $("#autoclosable-btn-success").prop("disabled", true);
    $(".alert-autocloseable-success").show();

    $(".alert-autocloseable-success")
      .delay(5000)
      .fadeOut("slow", function () {
        // Animation complete.
        $("#autoclosable-btn-success").prop("disabled", false);
      });
  });

  $("#normal-btn-success").click(function () {
    $(".alert-normal-success").show();
  });

  $(document).on("click", ".close", function () {
    $(this).parent().hide();
  });

  $("#btnConfirmFunction").click(function () {
    var x;
    if (confirm("Press a button!") == true) {
      x = "You pressed OK!";
    } else {
      x = "You pressed Cancel!";
    }
    document.getElementById("confirm-demo").innerHTML = x;
    $("#btnConfirmFunction").prop("disabled", true);
    $("#confirm-demo").show();

    $("#confirm-demo")
      .delay(5000)
      .fadeOut("slow", function () {
        // Animation complete.
        $("#btnConfirmFunction").prop("disabled", false);
      });
  });

  $("#btnAlertFunction").click(function () {
    var person = prompt("Please enter your name", "Enter name");

    if (person != null) {
      document.getElementById("prompt-demo").innerHTML =
        "You have entered '" + person + "' !";

      $("#btnAlertFunction").prop("disabled", true);
      $("#prompt-demo").show();

      $("#prompt-demo")
        .delay(5000)
        .fadeOut("slow", function () {
          // Animation complete.
          $("#btnAlertFunction").prop("disabled", false);
        });
    }
  });
});
